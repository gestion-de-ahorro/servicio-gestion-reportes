package com.gestion.de.ahorro.GestionReportes.entidades;

public class reporte {
    private Integer idReporte;
    private String fecha;
    private String consumo;
    private String detalle;
    private Double monto;
    private Double saldoMeta;

    public reporte() {
    }

    public reporte(Integer idReporte, String fecha, String consumo, String detalle, Double monto, Double saldoMeta) {
        this.idReporte = idReporte;
        this.fecha = fecha;
        this.consumo = consumo;
        this.detalle = detalle;
        this.monto = monto;
        this.saldoMeta = saldoMeta;
    }

    public Integer getIdReporte() {
        return idReporte;
    }

    public void setIdReporte(Integer idReporte) {
        this.idReporte = idReporte;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConsumo() {
        return consumo;
    }

    public void setConsumo(String consumo) {
        this.consumo = consumo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Double getSaldoMeta() {
        return saldoMeta;
    }

    public void setSaldoMeta(Double saldoMeta) {
        this.saldoMeta = saldoMeta;
    }

    @Override
    public String toString() {
        return "reporte{" + "idReporte=" + idReporte + ", fecha=" + fecha + ", consumo=" + consumo + ", detalle=" + detalle + ", monto=" + monto + ", saldoMeta=" + saldoMeta + '}';
    }

    
}
