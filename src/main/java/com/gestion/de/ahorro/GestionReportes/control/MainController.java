package com.gestion.de.ahorro.GestionReportes.control;



import java.net.URI;

import com.gestion.de.ahorro.GestionReportes.dao.dataReportes;
import com.gestion.de.ahorro.GestionReportes.entidades.logicaReporte;
import com.gestion.de.ahorro.GestionReportes.entidades.reporte;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@RestController
@RequestMapping(path = "/reportes")
public class MainController {
    @Autowired
    public dataReportes consultDao;

    @GetMapping(path="/", produces = "application/json")
    public logicaReporte getconsult() 
    {
        return consultDao.getAllconsult();
    }
    @PostMapping(path= "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addconsult(@RequestBody reporte consult){
        Integer id = consultDao.getAllconsult().getlistReportes().size() + 1;
        consult.setIdReporte(id);
        consultDao.addconsult(consult);
        
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                                    .path("/{idReporte}")
                                    .buildAndExpand(consult.getIdReporte())
                                    .toUri();
         
        return ResponseEntity.created(location).build();
    } 

}
