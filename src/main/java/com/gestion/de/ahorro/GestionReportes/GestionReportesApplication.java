package com.gestion.de.ahorro.GestionReportes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionReportesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionReportesApplication.class, args);
	}

}
