package com.gestion.de.ahorro.GestionReportes.dao;
import com.gestion.de.ahorro.GestionReportes.entidades.logicaReporte;
import com.gestion.de.ahorro.GestionReportes.entidades.reporte;
import org.springframework.stereotype.Repository;

@Repository
public class dataReportes {
    private static logicaReporte listReporte = new logicaReporte();
    
    static 
    {
        listReporte.getlistReportes().add(new reporte(1, "2021-02-28", "Pizzería Roma",
                "1 pizza familiar mas bebida", 25.00, 150.00));
        listReporte.getlistReportes().add(new reporte(2, "2021-03-15", "Vudu Bar",
                "3 michelada", 9.00, 141.00));
        listReporte.getlistReportes().add(new reporte(3, "2021-03-16", "ElectroCompu",
                "1 audifono - Headset - Pc", 42.60, 98.04));
        listReporte.getlistReportes().add(new reporte(4, "2021-03-22", "RM Boutique",
                "2 Camisa + Pantalon Jean", 50.00, 48.04));
        listReporte.getlistReportes().add(new reporte(5, "2021-03-26", "KFC",
                "1 combo alitas", 9.99, 38.41));
        listReporte.getlistReportes().add(new reporte(6, "2021-04-05", "Movil Store",
                "1 targeta sd", 12.99, 38.41));        
    }
     public logicaReporte getAllconsult() 
    {
        return listReporte;
    }
    public void addconsult(reporte consult) {
        listReporte.getlistReportes().add(consult);
    }
}
